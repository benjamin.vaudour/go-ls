package print

import (
	"fmt"
	"time"

	"framagit.org/benjamin.vaudour/go-ls/api/formats"
	"framagit.org/benjamin.vaudour/go-ls/api/git"
	"framagit.org/benjamin.vaudour/go-ls/api/options"
	"framagit.org/benjamin.vaudour/shell/file"
	"framagit.org/benjamin.vaudour/shell/output"
)

type lbFunc func(*file.File, ...string) line

type formater struct {
	options.Option
	datefmt       func(time.Time) string
	sizefmt       func(int64) string
	terminalWidth int
}

func newFormater(o options.Option, datefmt func(time.Time) string) formater {
	out := formater{
		Option:  o,
		datefmt: datefmt,
	}
	if o.Has(options.FormatHumanReadable) {
		out.sizefmt = formats.FormatSize
	} else {
		out.sizefmt = func(s int64) string { return fmt.Sprintf("%d", s) }
	}
	if ts, ok := output.GetTerminalSize(); ok {
		out.terminalWidth = ts.Width()
	}
	return out
}

func (lf formater) lineBuilder() lbFunc {
	olong := lf.Has(options.LongOption)
	ocolor := lf.HasNot(options.FormatNoColor)
	return func(f *file.File, forceName ...string) (l line) {
		if olong {
			l = append(l, newColumn(cPermission, formats.FormatPermissions(f)))
		}
		if lf.Has(options.OwnerOption) {
			l = append(l, newColumn(cOwner, f.Owner()))
		}
		if lf.Has(options.GroupOption) {
			l = append(l, newColumn(cGroup, f.Group()))
		}
		if olong {
			var date time.Time
			switch {
			case lf.Has(options.SortByCreation):
				date = f.CreationTime()
			case lf.Has(options.SortByAccess):
				date = f.AccessTime()
			default:
				date = f.ModificationTime()
			}
			l = append(l, newColumn(cDate, lf.datefmt(date)))
		}
		if lf.Has(options.SizeOption) {
			var size int64
			if lf.Has(options.DisplaySize) {
				size = f.BlockSize()
			} else {
				size = f.Size()
			}
			l = append(l, newColumn(cSize, lf.sizefmt(size)))
		}
		if lf.Has(options.DisplayGitStatus) {
			gitStatus := git.Status(f)
			var color string
			if ocolor {
				color = gitStatus.Color()
			}
			l = append(l, newColumn(cGit, string(gitStatus), color))
		}
		if lf.HasNot(options.FormatNoIcon) {
			var icon, color string
			if ocolor {
				icon, color = formats.FileIcon(f)
			} else {
				icon, _ = formats.FileIcon(f)
			}
			l = append(l, newColumn(cIcon, icon, color))
		}

		var fcolor, lcolor string
		link := f.LinkPath()
		if ocolor {
			fcolor = formats.FileColor(f)
			if f.IsSymlink() {
				lcolor = formats.LinkColor(link)
			}
		}
		var name string
		if len(forceName) > 0 {
			name = forceName[0]
		} else {
			name = f.Name()
		}
		l = append(l, newColumn(cName, name, fcolor))
		if link != "" {
			l = append(l, newColumn(cLink, link, lcolor))
		}
		return
	}
}

func (lf formater) getLines(lb lbFunc, files file.FileList, parent ...*file.File) (list lines, size int64) {
	list = newLines(lf)
	var p *file.File
	if len(parent) > 0 {
		p = parent[0]
	}
	needSize := lf.Has(options.SizeOption)
	var ss func(*file.File) int64
	if needSize {
		if lf.Has(options.DisplaySize) {
			ss = func(f *file.File) int64 { return f.BlockSize() }
		} else {
			ss = func(f *file.File) int64 { return f.Size() }
		}
	}
	if lf.Has(options.FilterAll) && p != nil {
		list.add(lb(p, "."))
		if needSize {
			size += ss(p)
		}
		if pp := p.Parent(); pp != nil {
			list.add(lb(pp, ".."))
		}
	}
	for _, f := range files {
		list.add(lb(f))
		if needSize {
			size += ss(f)
		}
	}
	return
}
