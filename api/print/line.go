package print

import (
	"fmt"
	"strings"
)

type line []column

func (l line) format(sizes map[columnType]int) line {
	wn, hasName := sizes[cName]
	wl, hasLink := sizes[cLink]
	in, il := -1, -1
	for i, c := range l {
		if c.tpe == cName {
			in = i
			continue
		}
		if c.tpe == cLink {
			il = i
			continue
		}
		if s, ok := sizes[c.tpe]; ok {
			l[i] = c.format(s)
		}
	}
	if hasName {
		wn0, wl0 := l[in].width(), 0
		if il >= 0 {
			wl0 = l[il].width()
		}
		if hasLink {
			if wl0 == 0 {
				l[in] = l[in].format(wn + wl + 3)
			} else {
				l[il] = l[il].format(wn + wl - wn0)
			}
		} else {
			l[in] = l[in].format(wn)
		}
	}
	return l
}

func (l line) String() string {
	s := len(l)
	hasLink := l[s-1].tpe == cLink
	if hasLink {
		s--
	}
	sc := make([]string, s)
	for i, c := range l[:s] {
		sc[i] = c.String()
	}
	if hasLink {
		sc[s-1] = fmt.Sprintf("%s → %s", sc[s-1], l[s].String())
	}
	return strings.Join(sc, " ")
}

func (l line) width() int {
	w := 0
	for i, c := range l {
		if i > 0 {
			w++
		}
		if c.tpe == cLink {
			w += 2
		}
		w += c.width()
	}
	return w
}
