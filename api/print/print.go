package print

import (
	"fmt"
	"time"

	"framagit.org/benjamin.vaudour/go-ls/api/options"
	"framagit.org/benjamin.vaudour/shell/file"
)

func printFiles(
	lf formater,
	lb lbFunc,
	files file.FileList,
	printParent bool,
	parent ...*file.File,
) {
	l, s := lf.getLines(lb, files, parent...)
	if len(parent) > 0 && printParent {
		pn := parent[0].Name()
		fmt.Printf("%s:\n", pn)
	}
	if lf.Has(options.SizeOption) {
		fmt.Printf("Total: %s\n", lf.sizefmt(s))
	}
	fmt.Println(l)
}

func Print(
	o options.Option,
	datefmt func(time.Time) string,
	files, dirs file.FileList,
	children []file.FileList,
) {
	lf := newFormater(o, datefmt)
	lb := lf.lineBuilder()
	hasFiles := len(files) > 0

	if hasFiles {
		printFiles(lf, lb, files, false)
	}

	if len(dirs) == 0 {
		return
	}

	if !hasFiles && len(dirs) == 1 {
		printFiles(lf, lb, children[0], false, dirs[0])
		return
	}

	for i, d := range dirs {
		if hasFiles || i > 0 {
			fmt.Println()
		}
		printFiles(lf, lb, children[i], true, d)
	}
}
