package print

import (
	"fmt"
	"strings"
	"unicode/utf8"

	"framagit.org/benjamin.vaudour/strutil/align"
)

type columnType uint

const (
	cName columnType = iota
	cLink
	cPermission
	cOwner
	cGroup
	cDate
	cSize
	cGit
	cIcon
)

var ts = map[columnType]string{
	cName:       "name",
	cLink:       "link",
	cPermission: "permission",
	cOwner:      "owner",
	cGroup:      "group",
	cDate:       "date",
	cSize:       "size",
	cGit:        "git",
	cIcon:       "icon",
}

func (t columnType) String() string {
	return ts[t]
}

var alignement = map[columnType]align.AlignmentType{
	cName:       align.LEFT,
	cLink:       align.LEFT,
	cPermission: align.LEFT,
	cOwner:      align.LEFT,
	cGroup:      align.LEFT,
	cDate:       align.RIGHT,
	cSize:       align.RIGHT,
	cGit:        align.LEFT,
	cIcon:       align.LEFT,
}

type column struct {
	tpe   columnType
	value string
	color string
}

func (c column) width() int { return utf8.RuneCountInString(c.value) }

func (c column) format(width int) column {
	if width <= 0 {
		c.value = ""
	} else {
		c.value = align.Align(c.value, alignement[c.tpe], width)
	}
	return c
}

func (c column) String() string {
	if c.color == "" {
		return c.value
	}
	v := strings.TrimLeft(c.value, " ")
	l := len(v)
	spl := strings.Repeat(" ", len(c.value)-l)
	v = strings.TrimRight(c.value, " ")
	spr := strings.Repeat(" ", l-len(v))
	return fmt.Sprintf("%s%s%s\033[m%s", spl, c.color, v, spr)
}

func newColumn(t columnType, value string, col ...string) column {
	c := column{
		tpe:   t,
		value: value,
	}
	if len(col) > 0 {
		c.color = col[0]
	}
	return c
}
