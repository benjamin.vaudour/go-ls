package print

import (
	"strings"

	"framagit.org/benjamin.vaudour/go-ls/api/options"
)

type lines struct {
	terminalWidth int
	col           bool
	files         []line
}

func (data lines) format() lines {
	width := make(map[columnType]int)
	for _, l := range data.files {
		for _, c := range l {
			wc := c.width()
			if w, ok := width[c.tpe]; !ok || wc > w {
				width[c.tpe] = wc
			}
		}
	}
	if w, ok := width[cLink]; ok && w == 0 {
		delete(width, cLink)
	}
	for i, l := range data.files {
		data.files[i] = l.format(width)
	}
	return data
}

func (data lines) list() (files []string) {
	files = make([]string, len(data.files))
	for i, c := range data.files {
		files[i] = c.String()
	}
	return
}

func (data lines) stringList() string {
	data = data.format()
	return strings.Join(data.list(), "\n")
}

func (data lines) stringCol() string {
	maxWidth := 1
	widths := make([]int, len(data.files))
	for i, f := range data.files {
		widths[i] = f.width()
		if widths[i] >= maxWidth {
			maxWidth = widths[i] + 1
		}
	}

	nbcols := data.terminalWidth / maxWidth
	if nbcols <= 1 {
		return data.stringList()
	}
	data = data.format()

	files := data.list()
	nblines, last := len(files)/nbcols, len(files)%nbcols
	if last > 0 {
		nblines++
	}

	cfiles := make([][]string, nblines)
	for i := range cfiles {
		nc := nbcols
		if i == nblines-1 && last > 0 {
			nc = last
		}
		cfiles[i] = make([]string, nc)
	}

	var c, l int
	for _, f := range files {
		cfiles[l][c] = f
		l++
		if l >= nblines || c >= len(cfiles[l]) {
			l = 0
			c++
		}
	}
	lfiles := make([]string, nblines)
	for i, line := range cfiles {
		lfiles[i] = strings.Join(line, " ")
	}
	return strings.Join(lfiles, "\n")
}

func (data lines) String() string {
	if data.col {
		return data.stringCol()
	}
	return data.stringList()
}

func newLines(lf formater) lines {
	return lines{
		terminalWidth: lf.terminalWidth,
		col:           lf.HasNot(options.LongOption | options.FormatColumn),
	}
}

func (data *lines) add(files ...line) {
	data.files = append(data.files, files...)
}
