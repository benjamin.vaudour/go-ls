package formats

import (
	"fmt"
)

func FormatSize(s int64) string {
	const unit int64 = 1024
	if s < unit {
		return fmt.Sprintf("%d", s)
	}
	var div, exp int64 = unit, 0
	for n := s / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	fs := float64(s) / float64(div)
	u := "KMGTPE"[exp]
	return fmt.Sprintf("%.1f%c", fs, u)
}
