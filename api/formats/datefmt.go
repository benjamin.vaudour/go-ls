package formats

import (
	"fmt"
	"strings"
	"time"
)

var (
	globalFormat = map[string]string{
		"":           time.ANSIC,
		"ANSIC":      time.ANSIC,
		"UnixDate":   time.UnixDate,
		"RubyDate":   time.RubyDate,
		"RFC822":     time.RFC822,
		"RFC822Z":    time.RFC822Z,
		"RFC850":     time.RFC850,
		"RFC1123":    time.RFC1123,
		"RFC1123W":   time.RFC1123Z,
		"RFC3339":    time.RFC3339,
		"Stamp":      time.Stamp,
		"StampMilli": time.StampMilli,
	}

	longWeekday = map[time.Weekday]string{
		time.Sunday:    "dimanche",
		time.Monday:    "lundi",
		time.Tuesday:   "mardi",
		time.Wednesday: "mercredi",
		time.Thursday:  "jeudi",
		time.Friday:    "vendredi",
		time.Saturday:  "samedi",
	}

	shortWeekday = map[time.Weekday]string{
		time.Sunday:    "dim.",
		time.Monday:    "lun.",
		time.Tuesday:   "mar.",
		time.Wednesday: "mer.",
		time.Thursday:  "jeu.",
		time.Friday:    "ven.",
		time.Saturday:  "sam.",
	}

	longMonth = map[time.Month]string{
		time.January:   "janvier",
		time.February:  "février",
		time.March:     "mars",
		time.April:     "avril",
		time.May:       "mai",
		time.June:      "juin",
		time.July:      "juillet",
		time.August:    "août",
		time.September: "septembre",
		time.October:   "octobre",
		time.November:  "novembre",
		time.December:  "décembre",
	}

	shortMonth = map[time.Month]string{
		time.January:   "jan.",
		time.February:  "fév.",
		time.March:     "mars",
		time.April:     "avr.",
		time.May:       "mai",
		time.June:      "juin",
		time.July:      "jui.",
		time.August:    "août",
		time.September: "sep.",
		time.October:   "oct.",
		time.November:  "nov.",
		time.December:  "déc.",
	}

	partialFormat = map[rune]func(time.Time) string{
		'%': func(t time.Time) string { return "%" },
		'a': func(t time.Time) string { return shortWeekday[t.Weekday()] },
		'A': func(t time.Time) string { return longWeekday[t.Weekday()] },
		'b': func(t time.Time) string { return shortMonth[t.Month()] },
		'B': func(t time.Time) string { return longMonth[t.Month()] },
		'c': func(t time.Time) string {
			wd := shortWeekday[t.Weekday()]
			md := fmt.Sprintf("%02d", t.Day())
			month := shortMonth[t.Month()]
			year := t.Year()
			h, m, s := t.Clock()
			tz, _ := t.Zone()
			return fmt.Sprintf("%s %s %s %s %02d:%02d:%02d %s", wd, md, month, year, h, m, s, tz)
		},
		'C': func(t time.Time) string { return fmt.Sprint(t.Year() / 100) },
		'd': func(t time.Time) string { return fmt.Sprintf("%02d", t.Day()) },
		'D': func(t time.Time) string {
			y, m, d := t.Date()
			return fmt.Sprintf("%02d/%02d/%02d", m, d, y%100)
		},
		'e': func(t time.Time) string { return fmt.Sprintf("% 2d", t.Day()) },
		'F': func(t time.Time) string {
			y, m, d := t.Date()
			return fmt.Sprintf("%04d-%02d-%02d", y, m, d)
		},
		'g': func(t time.Time) string {
			y, _ := t.ISOWeek()
			return fmt.Sprintf("%02d", y%100)
		},
		'G': func(t time.Time) string {
			y, _ := t.ISOWeek()
			return fmt.Sprint(y)
		},
		'h': func(t time.Time) string { return shortMonth[t.Month()] },
		'H': func(t time.Time) string { return fmt.Sprintf("%02d", t.Hour()) },
		'I': func(t time.Time) string {
			h := t.Hour() % 12
			if h == 0 {
				h = 12
			}
			return fmt.Sprintf("%02d", h)
		},
		'j': func(t time.Time) string { return fmt.Sprintf("%03d", t.YearDay()) },
		'k': func(t time.Time) string { return fmt.Sprintf("% 2d", t.Hour()) },
		'l': func(t time.Time) string {
			h := t.Hour() % 12
			if h == 0 {
				h = 12
			}
			return fmt.Sprintf("% 2d", h)
		},
		'm': func(t time.Time) string { return fmt.Sprintf("%02d", t.Month()) },
		'M': func(t time.Time) string { return fmt.Sprintf("%02d", t.Minute()) },
		'n': func(t time.Time) string { return "\n" },
		'N': func(t time.Time) string { return fmt.Sprintf("%09d", t.Nanosecond()) },
		'p': func(t time.Time) string {
			h := t.Hour()
			if h >= 12 {
				return "PM"
			}
			return "AM"
		},
		'P': func(t time.Time) string {
			h := t.Hour()
			if h >= 12 {
				return "pm"
			}
			return "am"
		},
		'q': func(t time.Time) string { return fmt.Sprintf("%d", (t.Month()-1)/3+1) },
		'r': func(t time.Time) string {
			h, m, s := t.Clock()
			p := "PM"
			if h < 12 {
				p = "AM"
			}
			h = h % 12
			if h == 0 {
				h = 12
			}
			return fmt.Sprintf("%02d:%02d:%02d %s", h, m, s, p)
		},
		'R': func(t time.Time) string {
			h, m, _ := t.Clock()
			return fmt.Sprintf("%02d:%02d", h, m)
		},
		's': func(t time.Time) string { return fmt.Sprint(t.Unix()) },
		'S': func(t time.Time) string { return fmt.Sprintf("%02d", t.Second()) },
		't': func(t time.Time) string { return "\t" },
		'T': func(t time.Time) string {
			h, m, s := t.Clock()
			return fmt.Sprintf("%02d:%02d:%02d", h, m, s)
		},
		'u': func(t time.Time) string {
			d := t.Weekday()
			if d == 0 {
				d = 7
			}
			return fmt.Sprintf("%d", d)
		},
		'U': func(t time.Time) string {
			_, w := t.ISOWeek()
			if t.Weekday() == time.Sunday {
				w++
			}
			return fmt.Sprintf("%02d", w)
		},
		'V': func(t time.Time) string {
			_, w := t.ISOWeek()
			if w == 0 {
				w = 53
			}
			return fmt.Sprintf("%02d", w)
		},
		'w': func(t time.Time) string { return fmt.Sprintf("%d", t.Weekday()) },
		'W': func(t time.Time) string {
			_, w := t.ISOWeek()
			return fmt.Sprintf("%02d", w)
		},
		'x': func(t time.Time) string {
			y, m, d := t.Date()
			return fmt.Sprintf("%02d/%02d/%04d", d, m, y)
		},
		'y': func(t time.Time) string { return fmt.Sprintf("%02d", t.Year()%100) },
		'Y': func(t time.Time) string { return fmt.Sprint(t.Year()) },
		'z': func(t time.Time) string {
			_, tz := t.Zone()
			s := ""
			if tz < 0 {
				s, tz = "-", -tz
			}
			h, m := tz/3600, (tz%3600)/60
			return fmt.Sprintf("%s%02d%02d", s, h, m)
		},
		'Z': func(t time.Time) string {
			tz, _ := t.Zone()
			return tz
		},
	}
)

func IsDateFormatValid(f string) bool {
	if _, ok := globalFormat[f]; ok {
		return true
	}
	if f[0] != '+' {
		return false
	}
	runes := []rune(f[1:])
	for len(runes) > 0 {
		r := runes[0]
		runes = runes[1:]
		if r == '%' {
			if len(runes) == 0 {
				return false
			}
			r = runes[0]
			runes = runes[1:]
			if _, ok := partialFormat[r]; !ok {
				return false
			}
		}
	}
	return true
}

func DateFormat(f string) func(time.Time) string {
	if l, ok := globalFormat[f]; ok {
		return func(t time.Time) string { return t.Format(l) }
	}
	return func(t time.Time) string {
		var b strings.Builder
		runes := []rune(f[1:])
		for len(runes) > 0 {
			r := runes[0]
			runes = runes[1:]
			if r == '%' {
				r = runes[0]
				runes = runes[1:]
				b.WriteString(partialFormat[r](t))
			} else {
				b.WriteRune(r)
			}
		}
		return b.String()
	}
}

func GetDateFormaterFunc(f string) (callback func(time.Time) string, isValid bool) {
	return DateFormat(f), IsDateFormatValid(f)
}
