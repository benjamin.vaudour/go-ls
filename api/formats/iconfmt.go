package formats

import (
	"strings"

	"framagit.org/benjamin.vaudour/go-ls/api/icons"
	"framagit.org/benjamin.vaudour/shell/file"
)

func FileIcon(f *file.File) (icon, color string) {
	name, _ := f.Split()
	name = strings.ToLower(name)
	ext := strings.ToLower(f.Extension())
	hidden, exe := strings.HasPrefix(name, "."), f.HasMode(0700)
	var i *icons.Icon_Info
	var ok bool
	if f.IsDir() {
		if i, ok = icons.Icon_Dir[name]; !ok {
			if hidden {
				i = icons.Icon_Def["hiddendir"]
			} else {
				i = icons.Icon_Def["dir"]
			}
		}
	} else if i, ok = icons.Icon_FileName[name]; !ok {
		if i, ok = icons.Icon_Ext[ext]; !ok {
			if hidden {
				i = icons.Icon_Def["hiddenfile"]
				if exe {
					i.MakeExe()
				}
			} else if exe {
				i = icons.Icon_Def["exe"]
			} else {
				i = icons.Icon_Def["file"]
			}
		}
	}
	return i.GetGlyph(), i.GetColor(1)
}
