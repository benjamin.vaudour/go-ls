package formats

import (
	"os"
	"path"

	"framagit.org/benjamin.vaudour/go-ls/api/colors"
	"framagit.org/benjamin.vaudour/shell/file"
)

func getColor(fm os.FileMode, ext string) string {
	color := colors.GetColorType(fm)
	if color != "" {
		return color
	}
	return colors.GetColorExtension(ext)
}

func FileColor(f *file.File) string {
	return getColor(f.Mode(), f.Extension())
}

func LinkColor(link string) string {
	if link == "" {
		return ""
	}
	lf, err := os.Stat(link)
	if err != nil {
		color, _ := colors.GetOrphan()
		return color
	}
	ext := path.Ext(link)
	return getColor(lf.Mode(), ext)
}
