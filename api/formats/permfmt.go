package formats

import (
	"framagit.org/benjamin.vaudour/shell/file"
)

func FormatPermissions(f *file.File) string {
	fm := f.Mode()
	perm := []rune(fm.Perm().String())
	switch {
	case f.IsSymlink():
		perm[0] = 'l'
	case f.IsDir():
		perm[0] = 'd'
	case f.IsCharDevice():
		perm[0] = 'c'
	case f.IsDevice():
		perm[0] = 'b'
	case f.IsPipe():
		perm[0] = 'p'
	case f.IsSocket():
		perm[0] = 's'
	case f.IsIrregular():
		perm[0] = '?'
	}
	if f.IsUid() {
		perm[3] = 's'
	}
	if f.IsGid() {
		perm[6] = 's'
	}
	if f.IsSticky() {
		perm[9] = 't'
	}
	return string(perm)
}
