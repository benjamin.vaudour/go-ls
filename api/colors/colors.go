package colors

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

var mcolors map[string]string

func getColorEnv() string { return os.Getenv("LS_COLORS") }

func setColorEnv(colors string) bool { return os.Setenv("LS_COLORS", colors) == nil }

func existDircolors(dircolors string) bool {
	f, err := os.Stat(dircolors)
	if err != nil {
		return false
	}
	return f.Mode().IsRegular()
}

func dircolorsExec(args ...string) (out string, ok bool) {
	cmd := exec.Command("dircolors", args...)
	output, err := cmd.Output()
	if ok = err == nil; ok {
		var w strings.Builder
		w.Write(output)
		out = w.String()
	}
	return
}

func getDircolors(dircolors string) (out string, ok bool) {
	if existDircolors(dircolors) {
		if out, ok = dircolorsExec("-b", dircolors); ok {
			return
		}
	}
	return dircolorsExec("-b")
}

func setDircolors(dircolors string) bool {
	out, ok := getDircolors(dircolors)
	if !ok {
		return false
	}
	if !strings.HasPrefix(out, "LS_COLORS=") {
		return false
	}
	value := strings.TrimPrefix(out, "LS_COLORS=")
	value = strings.Trim(value, "'\"")
	return setColorEnv(value)
}

func initMap() bool {
	colors := strings.Split(getColorEnv(), ":")
	mcolors = make(map[string]string)
	for _, c := range colors {
		kv := strings.Split(c, "=")
		if len(kv) != 2 {
			continue
		}
		mcolors[kv[0]] = kv[1]
	}
	return len(mcolors) > 0
}

func InitColors(erase bool, dircolors ...string) bool {
	if !erase && mcolors != nil && len(mcolors) > 0 {
		return true
	}
	if erase || getColorEnv() == "" {
		dc := ""
		if len(dircolors) > 0 {
			dc = dircolors[0]
		}
		if !setDircolors(dc) {
			return false
		}
	}
	return initMap()
}

func init() {
	InitColors(true)
}

func is(fm os.FileMode, m os.FileMode) bool { return fm&m > 0 }

func get(k string) (color string, ok bool) {
	color, ok = mcolors[k]
	if ok {
		color = fmt.Sprintf("\033[%sm", color)
	}
	return
}

func GetDir() (string, bool)                 { return get("di") }
func GetLink() (string, bool)                { return get("ln") }
func GetMultiHardlink() (string, bool)       { return get("mh") }
func GetPipe() (string, bool)                { return get("pi") }
func GetSocket() (string, bool)              { return get("so") }
func GetDoor() (string, bool)                { return get("do") }
func GetBlockDevice() (string, bool)         { return get("bd") }
func GetCharDevice() (string, bool)          { return get("cd") }
func GetOrphan() (string, bool)              { return get("or") }
func GetMissing() (string, bool)             { return get("mi") }
func GetSetuid() (string, bool)              { return get("su") }
func GetSetgid() (string, bool)              { return get("sg") }
func GetCapability() (string, bool)          { return get("ca") }
func GetStickyOtherWritable() (string, bool) { return get("tw") }
func GetSticky() (string, bool)              { return get("st") }
func GetOtherWritable() (string, bool)       { return get("ow") }
func GetExecutable() (string, bool)          { return get("ex") }

func GetColorType(fm os.FileMode) (color string) {
	var ok bool
	order := []os.FileMode{
		os.ModeSymlink,
		os.ModeNamedPipe,
		os.ModeSocket,
		os.ModeCharDevice,
		os.ModeDevice,
	}
	action := map[os.FileMode]func() (string, bool){
		os.ModeSymlink:    GetLink,
		os.ModeNamedPipe:  GetPipe,
		os.ModeSocket:     GetSocket,
		os.ModeCharDevice: GetCharDevice,
		os.ModeDevice:     GetBlockDevice,
	}
	for _, m := range order {
		if is(fm, m) {
			if color, ok = action[m](); ok {
				return
			}
		}
	}
	if fm.IsDir() {
		ow := is(fm, 0002)
		st := is(fm, os.ModeSticky)
		if st && ow {
			if color, ok = GetStickyOtherWritable(); ok {
				return
			}
		}
		if st {
			if color, ok = GetSticky(); ok {
				return
			}
		}
		if ow {
			if color, ok = GetOtherWritable(); ok {
				return
			}
		}
		color, ok = GetDir()
		return
	} else if fm.IsRegular() {
		order = []os.FileMode{
			os.ModeAppend | os.ModeExclusive, //Capabilities ???
			os.ModeSetuid,
			os.ModeSetgid,
			0100,
		}
		action = map[os.FileMode]func() (string, bool){
			os.ModeAppend | os.ModeExclusive: GetCapability, //Capabilities ???
			os.ModeSetuid:                    GetSetuid,
			os.ModeSetgid:                    GetSetgid,
			0100:                             GetExecutable,
		}
		for _, m := range order {
			if !is(fm, m) {
				continue
			}
			if color, ok = action[m](); ok {
				return
			}
		}
	}
	return
}

func GetColorExtension(extension string) string {
	color, _ := get("*." + extension)
	return color
}
