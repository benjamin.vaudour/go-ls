package options

import (
	"os"

	"framagit.org/benjamin.vaudour/shell/command/flag"
)

var (
	flags                   *flag.FlagSet
	fHelp                   bool
	fVersion                bool
	fFilterAll              bool
	fFilterAlmostAll        bool
	fFilterIgnoreBackup     bool
	fFilterDirectoryContent bool
	fFilterRecursive        bool
	fSortDirectoryFirst     bool
	fSortByCreation         bool
	fSortByModification     bool
	fSortByAccess           bool
	fSortByName             bool = true
	fSortNatural            bool
	fSortByExtension        bool
	fSortNone               bool
	fSortReverse            bool
	fFormatColumn           bool
	fFormatNoColor          bool
	fFormatNoIcon           bool
	fFormatHumanReadable    bool
	fFormatTime             string
	fDisplayGroup           bool
	fDisplayLongFormat      bool
	fDisplayOwner           bool
	fDisplaySize            bool
	fDisplayGitStatus       bool
	args                    []string
)

var mapFlags = map[*bool]Option{
	&fHelp:                   Help,
	&fVersion:                Version,
	&fFilterAll:              FilterAll,
	&fFilterAlmostAll:        FilterAlmostAll,
	&fFilterIgnoreBackup:     FilterIgnoreBackup,
	&fFilterDirectoryContent: FilterDirectoryContent,
	&fFilterRecursive:        FilterRecursive,
	&fSortDirectoryFirst:     SortDirectoryFirst,
	&fSortByCreation:         SortByCreation,
	&fSortByModification:     SortByModification,
	&fSortByAccess:           SortByAccess,
	&fSortByName:             SortByName,
	&fSortNatural:            SortNatural,
	&fSortByExtension:        SortByExtension,
	&fSortNone:               SortNone,
	&fSortReverse:            SortReverse,
	&fFormatColumn:           FormatColumn,
	&fFormatNoColor:          FormatNoColor,
	&fFormatNoIcon:           FormatNoIcon,
	&fFormatHumanReadable:    FormatHumanReadable,
	&fDisplayGroup:           DisplayGroup,
	&fDisplayLongFormat:      DisplayLongFormat,
	&fDisplayOwner:           DisplayOwner,
	&fDisplaySize:            DisplaySize,
	&fDisplayGitStatus:       DisplayGitStatus,
}

func init() {
	flags = flag.NewSet(true)
	flags.Add(
		flag.Bool("-?", "Affiche cette aide", &fHelp, false, true, "--help"),
		flag.Bool("--version", "Affiche la version de l’application", &fVersion, false, true),
		flag.Bool("-a", "Affiche les fichiers cachés", &fFilterAll, false, true, "--all"),
		flag.Bool("-A", "Comme -a mais sans . et ..", &fFilterAlmostAll, false, true, "--almost-all"),
		flag.Bool("-B", "Ignore des fichiers se terminant par ~", &fFilterIgnoreBackup, false, true, "--ignore-backups"),
		flag.Bool("-R", "Affiche récursivement les sous-répertoires", &fFilterRecursive, false, true, "--recursive"),
		flag.Bool("-d", "Affiche le nom du répertoire, pas son contenu", &fFilterDirectoryContent, false, true, "--directory"),
		flag.Bool("--group-directories-first", "Regroupe les répertoires avant les fichiers", &fSortDirectoryFirst, false, true),
		flag.Bool("-t", "Trie par date de modification", &fSortByModification, false, true),
		flag.Bool("-c", "Trie par date de création", &fSortByCreation, false, true),
		flag.Bool("-u", "Trie par date d’accès", &fSortByAccess, false, true),
		flag.Bool("-v", "Trie de façon naturelle (pour les numéros)", &fSortNatural, false, true),
		flag.Bool("-X", "Trie par nom d’extension", &fSortByExtension, false, true),
		flag.Bool("-U", "Aucun tri", &fSortNone, false, true),
		flag.Bool("-r", "Inverse l’ordre du tri", &fSortReverse, false, true, "--reverse"),
		flag.Bool("-1", "Affiche les résultats sur une seule colonne", &fFormatColumn, false, true),
		flag.Bool("--no-color", "N’Affiche pas en couleurs", &fFormatNoColor, false, true),
		flag.Bool("--no-icon", "N’affiche pas les icônes", &fFormatNoIcon, false, true),
		flag.Bool("-h", "Formate les tailles au format humain", &fFormatHumanReadable, false, true, "--human-readable"),
		flag.String("-T", "Format de dates (voir plus bas pour les valeurs possibles)", &fFormatTime, "ANSIC", true, "--time-style"),
		flag.Bool("-g", "Affiche le groupe, pas le propriétaire", &fDisplayGroup, false, true),
		flag.Bool("-G", "Affiche le propriétaire, pas le groupe", &fDisplayOwner, false, true),
		flag.Bool("-l", "Affiche au format long", &fDisplayLongFormat, false, true),
		flag.Bool("-s", "Affiche la taille d’allocation en bloc", &fDisplaySize, false, true, "--size"),
		flag.Bool("--git", "Affiche le statut git du fichier", &fDisplayGitStatus, false, true),
	)
	flags.AddArgs(flag.NewArg("<fichiers>", "Fichiers à rechercher", -1, &args))
}

func GetOptions() Option {
	var o Option
	for f, of := range mapFlags {
		if *f {
			o |= of
		}
	}
	return o
}

func GetTimeFormat() string {
	return fFormatTime
}

func GetVersion() string {
	return AppVersion
}

func GetArgs() []string {
	return args
}

func Parse() error {
	_, err := flags.Parse(os.Args[1:])
	return err
}

func GetHelp() string {
	return flags.Help() + `

Formats de date:
 ANSIC       "Mon Jan _2 15:04:05 2006"      
 UnixDate    "Mon Jan _2 15:04:05 MST 2006"  
 RubyDate    "Mon Jan 02 15:04:05 -0700 2006"
 RFC822      "02 Jan 06 15:04 MST"           
 RFC822Z     "02 Jan 06 15:04 -0700"         
 RFC850      "Monday, 02-Jan-06 15:04:05 MST"
 RFC1123     "Mon, 02 Jan 2006 15:04:05 MST" 
 RFC1123Z    "Mon, 02 Jan 2006 15:04:05 -0700"
 RFC3339     "2006-01-02T15:04:05Z07:00"     
 Kitchen     "3:04PM"                        
 Stamp       "Mon Jan _2 15:04:05"            [Default]
 StampMilli  "Jan _2 15:04:05.000"
 +<motif> où motif peut contenir les substitutions suivantes:
       %%     a literal %
       %a     locale's abbreviated weekday name (e.g., Sun)
       %A     locale's full weekday name (e.g., Sunday)
       %b     locale's abbreviated month name (e.g., Jan)
       %B     locale's full month name (e.g., January)
       %c     locale's date and time (e.g., Thu Mar  3 23:05:25 2005)
       %C     century; like %Y, except omit last two digits (e.g., 20)
       %d     day of month (e.g., 01)
       %D     date; same as %m/%d/%y
       %e     day of month, space padded; same as %_d
       %F     full date; like %+4Y-%m-%d
       %g     last two digits of year of ISO week number (see %G)
       %G     year of ISO week number (see %V); normally useful only with %V
       %h     same as %b
       %H     hour (00..23)
       %I     hour (01..12)
       %j     day of year (001..366)
       %k     hour, space padded ( 0..23); same as %_H
       %l     hour, space padded ( 1..12); same as %_I
       %m     month (01..12)
       %M     minute (00..59)
       %n     a newline
       %N     nanoseconds (000000000..999999999)
       %p     locale's equivalent of either AM or PM; blank if not known
       %P     like %p, but lower case
       %q     quarter of year (1..4)
       %r     locale's 12-hour clock time (e.g., 11:11:04 PM)
       %R     24-hour hour and minute; same as %H:%M
       %s     seconds since 1970-01-01 00:00:00 UTC
       %S     second (00..60)
       %t     a tab
       %T     time; same as %H:%M:%S
       %u     day of week (1..7); 1 is Monday
       %U     week number of year, with Sunday as first day of week (00..53)
       %V     ISO week number, with Monday as first day of week (01..53)
       %w     day of week (0..6); 0 is Sunday
       %W     week number of year, with Monday as first day of week (00..53)
       %x     locale's date representation (e.g., 12/31/99)
       %X     locale's time representation (e.g., 23:13:48)
       %y     last two digits of year (00..99)
       %Y     year
       %z     +hhmm numeric time zone (e.g., -0400)
       %:z    +hh:mm numeric time zone (e.g., -04:00)
       %::z   +hh:mm:ss numeric time zone (e.g., -04:00:00)
       %:::z  numeric time zone with : to necessary precision (e.g., -04, +05:30)
       %Z     alphabetic time zone abbreviation (e.g., EDT)`
}
