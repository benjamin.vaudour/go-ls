package options

type Option uint

const (
	Help Option = 1 << iota
	Version
	FilterAll
	FilterAlmostAll
	FilterIgnoreBackup
	FilterDirectoryContent
	FilterRecursive
	SortDirectoryFirst
	SortByCreation
	SortByModification
	SortByAccess
	SortByName
	SortNatural
	SortNone
	SortByExtension
	SortReverse
	FormatColumn
	FormatNoColor
	FormatNoIcon
	FormatHumanReadable
	DisplayGroup
	DisplayLongFormat
	DisplayOwner
	DisplaySize
	DisplayGitStatus

	LongOption       = DisplayLongFormat | DisplayGroup | DisplayOwner
	OwnerOption      = DisplayLongFormat | DisplayOwner
	GroupOption      = DisplayLongFormat | DisplayGroup
	SizeOption       = LongOption | DisplaySize
	SortByDateOption = SortByAccess | SortByCreation | SortByModification
	HiddenOption     = FilterAll | FilterAlmostAll

	AppVersion = "1.1.0"
)

func (o Option) Has(o2 Option) bool {
	return o&o2 > 0
}

func (o Option) HasNot(o2 Option) bool {
	return !o.Has(o2)
}
