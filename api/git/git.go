package git

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"framagit.org/benjamin.vaudour/shell/file"
)

type State string

const (
	Unmodified State = ""
	Deleted    State = "D"
	Added      State = "A" // ou "??"
	Modified   State = "M"
	Conflict   State = "C" // "UU"
	DirUpdated State = "●"
)

var (
	gitPath  = make(map[string]bool)
	gitState = make(map[string]State)
	color    = map[State]string{
		Deleted:    "1;31",
		Added:      "1;34",
		Modified:   "32",
		Conflict:   "31",
		DirUpdated: "33",
	}
)

func (st State) Color() string {
	if c, ok := color[st]; ok {
		return fmt.Sprintf("\033[%sm", c)
	}
	return ""
}

func updateGitStatusDir(base, fpath string) {
	d := filepath.Dir(fpath)
	for d != base {
		gitState[d] = DirUpdated
		d = filepath.Dir(d)
		if _, ok := gitState[d]; ok {
			break
		}
	}
}

func updateGitStatus(dirname string) {
	if err := os.Chdir(dirname); err != nil {
		return
	}
	cmd := exec.Command("git", "status", "-s")
	out, err := cmd.StdoutPipe()
	if err != nil {
		return
	}
	defer out.Close()
	sc := bufio.NewScanner(out)
	if err := cmd.Start(); err != nil {
		return
	}
	u := false
	for sc.Scan() {
		u = true
		line := sc.Text()
		status, name := strings.TrimSpace(line[:3]), strings.TrimSpace(line[3:])
		fpath := filepath.Join(dirname, name)
		switch status {
		case "??", "A":
			gitState[fpath] = Added
		case "UU":
			gitState[fpath] = Conflict
		default:
			gitState[fpath] = State(status)
		}
		updateGitStatusDir(dirname, fpath)
	}
	if u {
		gitState[dirname] = DirUpdated
	}
	cmd.Wait()
}

func searchGitInfo(f *file.File) bool {
	apath := f.AbsolutePath()
	if done := gitPath[apath]; done {
		return false
	}
	if !f.IsDir() {
		return true
	}
	for _, c := range f.Children() {
		if c.Name() == ".git" {
			gitPath[apath] = true
			updateGitStatus(apath)
			return false
		}
	}
	d, err := os.Open(apath)
	if err != nil {
		gitPath[apath] = true
		return false
	}
	defer d.Close()
	children, _ := d.Readdir(-1)
	for _, c := range children {
		if c.Name() == ".git" {
			gitPath[apath] = true
			updateGitStatus(apath)
			return false
		}
	}
	gitPath[apath] = true
	return true
}

func updateGitInfo(f *file.File) {
	for searchGitInfo(f) {
		f = f.SearchParent()
		if f == nil {
			break
		}
	}
}

func Status(f *file.File) State {
	apath := f.AbsolutePath()
	if st, ok := gitState[apath]; ok {
		return st
	}
	updateGitInfo(f)
	return gitState[apath]
}
