package search

import (
	"path/filepath"
	"sync"

	"framagit.org/benjamin.vaudour/go-ls/api/options"
	"framagit.org/benjamin.vaudour/shell/file"
)

func searchData(args []string, o options.Option) (files, dirs file.FileList, root map[string]*file.File) {
	root = make(map[string]*file.File)

	ignoreChildren := o.Has(options.FilterDirectoryContent)
	recursive := o.Has(options.FilterRecursive)
	keepHidden := o.Has(options.HiddenOption)
	keepBackup := o.HasNot(options.FilterIgnoreBackup)

	deepness := 1
	if ignoreChildren {
		deepness = 0
	} else if recursive {
		deepness = -1
	}

	var wg sync.WaitGroup
	for _, e := range args {
		dirname := "."
		if filepath.IsAbs(e) {
			dirname = "/"
		}
		f, err := file.New(dirname, e)
		if err != nil {
			continue
		}
		if !f.IsDir() || ignoreChildren {
			files.Add(f)
		} else {
			root[f.AbsolutePath()] = f
			dirs.Add(f)
			wg.Add(1)
			go (func(f *file.File) {
				defer wg.Done()
				f.SearchChildren(deepness, keepHidden, keepBackup)
			})(f)
		}
	}
	wg.Wait()
	return
}

func setRecursive(dirs file.FileList) (alldirs file.FileList) {
	for _, d := range dirs {
		alldirs.Add(d.Flatten(true)...)
	}
	return
}

func searchParent(dirs file.FileList, root map[string]*file.File) {
	for _, d := range dirs {
		if d.Parent() != nil {
			continue
		}
		ppath, _ := d.AbsoluteSplit()
		ppath = filepath.Clean(ppath)
		if p, ok := root[ppath]; ok {
			d.SetParent(p)
			continue
		}
		p, err := file.New(ppath, ".")
		if err == nil {
			d.SetParent(p)
			root[ppath] = p
		}
	}
}

func getSortFunc(o options.Option) (cb func(file.FileList) file.FileList) {
	reverse := o.Has(options.SortReverse)

	var cmps []file.CmpFunc
	if o.Has(options.SortDirectoryFirst) {
		cmps = append(cmps, file.CmpDir)
	}
	switch {
	case o.Has(options.SortByDateOption):
		switch {
		case o.Has(options.SortByCreation):
			cmps = append(cmps, file.CmpCreationTime)
		case o.Has(options.SortByAccess):
			cmps = append(cmps, file.CmpAccessTime)
		case o.Has(options.SortByModification):
			cmps = append(cmps, file.CmpModificationTime)
		}
	case o.Has(options.SortByExtension):
		cmps = append(cmps, file.CmpExt)
	case o.Has(options.DisplaySize):
		cmps = append(cmps, file.CmpBlockSize)
	}
	if o.HasNot(options.SortNone) {
		if o.Has(options.SortNatural) {
			cmps = append(cmps, file.CmpNatural)
		} else {
			cmps = append(cmps, file.CmpName)
		}
	}
	if len(cmps) == 0 {
		if reverse {
			return func(fl file.FileList) file.FileList {
				l := len(fl)
				l2 := l >> 1
				for i := 0; i < l2; i++ {
					j := l - 1 - i
					fl[i], fl[j] = fl[j], fl[i]
				}
				return fl
			}
		}
		return func(fl file.FileList) file.FileList { return fl }
	}
	cmp := file.CmpAll(cmps...)
	if reverse {
		cmp = file.ReverseCmp(cmp)
	}
	return func(fl file.FileList) file.FileList {
		return fl.Sort(cmp)
	}
}

func sortAll(files, dirs file.FileList, o options.Option) []file.FileList {
	cb := getSortFunc(o)
	var wg sync.WaitGroup
	cbs := func(l file.FileList) {
		defer wg.Done()
		cb(l)
	}
	wg.Add(2)
	go cbs(files)
	go cbs(dirs)
	wg.Wait()
	children := make([]file.FileList, len(dirs))
	wg.Add(len(dirs))
	for i, p := range dirs {
		go (func(i int, p *file.File) {
			defer wg.Done()
			children[i] = cb(p.Children())
		})(i, p)
	}
	wg.Wait()
	return children
}

func Search(args []string, o options.Option) (files, dirs file.FileList, children []file.FileList) {
	var root map[string]*file.File
	files, dirs, root = searchData(args, o)

	if len(dirs) > 0 && o.Has(options.FilterAll) {
		searchParent(dirs, root)
	}

	if len(dirs) > 0 && o.Has(options.FilterRecursive) {
		dirs = setRecursive(dirs)
	}

	children = sortAll(files, dirs, o)
	return
}
