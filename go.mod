module framagit.org/benjamin.vaudour/go-ls

go 1.15

require (
	framagit.org/benjamin.vaudour/shell v1.4.4
	framagit.org/benjamin.vaudour/strutil v1.1.0
)
