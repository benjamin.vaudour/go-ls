package main

import (
	"fmt"
	"os"

	"framagit.org/benjamin.vaudour/go-ls/api/formats"
	"framagit.org/benjamin.vaudour/go-ls/api/options"
	"framagit.org/benjamin.vaudour/go-ls/api/print"
	"framagit.org/benjamin.vaudour/go-ls/api/search"
)

var (
	Version = "1.0"
)

func main() {
	err := options.Parse()
	if err != nil {
		fmt.Fprintln(os.Stderr, err, "\n")
		fmt.Fprintln(os.Stderr, options.GetHelp())
		os.Exit(1)
	}

	o := options.GetOptions()
	if o.Has(options.Help) {
		fmt.Fprintln(os.Stderr, options.GetHelp())
		return
	}
	if o.Has(options.Version) {
		fmt.Fprintln(os.Stderr, options.GetOptions())
		return
	}

	timefmt, valid := formats.GetDateFormaterFunc(options.GetTimeFormat())
	if !valid {
		fmt.Fprintln(os.Stderr, "Format de date invalide !\n")
		fmt.Fprintln(os.Stderr, options.GetHelp())
		os.Exit(1)
	}

	args := options.GetArgs()
	if len(args) == 0 {
		args = append(args, ".")
	}

	files, dirs, children := search.Search(args, o)
	print.Print(o, timefmt, files, dirs, children)
}
